package com.aoc;

import com.aoc.day5.Challenge1and2Refactor;
import com.aoc.day13.Challenge1and2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();
        new Challenge1and2().challenge1();
        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        System.out.println("Execution time: " + TimeUnit.MILLISECONDS.convert(duration, TimeUnit.NANOSECONDS) + "ms");
    }
}
