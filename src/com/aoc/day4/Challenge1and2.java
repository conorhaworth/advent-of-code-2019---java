package com.aoc.day4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Challenge1and2 {


//    It is a six-digit number.
//    The value is within the range given in your puzzle input.
//    Two adjacent digits are the same (like 22 in 122345).
//    Going from left to right, the digits never decrease; they only ever increase or stay the same (like 111123 or 135679).


    public int challenge1() {
        var lowerBound = 359282;
        var upperBound = 820401;

        //too low - 315

        var totalPossiblePasswords = new ArrayList<Integer>();
        for (int i = lowerBound; i < upperBound; i++) {
            var digits = String.valueOf(i).split("");
            var doesDecrease = false;
            var adjacentDigits = false;
            var repeatedDigits = new HashMap<String, Integer>();
            for (int j = 0; j < digits.length; j++) {
                var currentDigit = digits[j];
                if (repeatedDigits.containsKey(currentDigit)) {
                    repeatedDigits.put(currentDigit, repeatedDigits.get(currentDigit) + 1);
                } else {
                    repeatedDigits.put(currentDigit, 1);
                }

                if (j > 0) {
                    for (int x = 0; x < j; x++) {
                        if (Integer.parseInt(currentDigit) < Integer.parseInt(digits[x])) {
                            doesDecrease = true;
                            break;
                        }
                    }
                }
            }
            adjacentDigits = repeatedDigits.entrySet().stream().anyMatch(r -> r.getValue() == 2);


            if (adjacentDigits && !doesDecrease) {
                System.out.println(i);
                totalPossiblePasswords.add(i);
            }
        }

        return totalPossiblePasswords.size();
    }
}
