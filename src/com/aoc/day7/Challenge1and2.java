package com.aoc.day7;

import com.aoc.IntCode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.aoc.FileReader.readFile;

public class Challenge1and2 {

    public void challenge1() throws Exception {
        String[] inputString = readFile("day7").get(0).split(",");
        var intCode = Arrays.stream(inputString).map(Integer::parseInt).collect(Collectors.toList());
        List<int[]> permutations = new ArrayList<>();

        for (int i = 0; i <= 44444; i++) {
            var currentPermutation = String.valueOf(i);
            var currentLength = currentPermutation.length();
            if (currentLength < 5) {
                for (int j = 0; j < 5 - currentLength; j++) {
                    currentPermutation = "0" + currentPermutation;
                }
            }
            var settings = currentPermutation.split("");
            int[] phaseSetting = {Integer.parseInt(settings[0]),
                    Integer.parseInt(settings[1]),
                    Integer.parseInt(settings[2]),
                    Integer.parseInt(settings[3]),
                    Integer.parseInt(settings[4])};
            permutations.add(phaseSetting);
        }

        for (int[] permutation : permutations) {
            var output = 0;
            for (int i = 0; i < 5; i++) {
//                output = new IntCode(intCode).run(permutation[i]);
            }
        }
        System.out.println("fasf");
    }
}
