package com.aoc.day11;

import com.aoc.FileReader;
import com.aoc.IntCode;

import java.awt.*;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class Challenge1and2 {

    public void challenge1() throws IOException {
        List<Long> memory = FileReader.getIntCodeInput("day11");
        ArrayBlockingQueue<Long> input = new ArrayBlockingQueue<>(10);
        ArrayBlockingQueue<Long> output = new ArrayBlockingQueue<>(10);
        HullPaintingRobot robotThread = new HullPaintingRobot(input, output);
        IntCode intCode = new IntCode(memory, input, output);

        intCode.start();
        robotThread.start();
    }
}

class HullPaintingRobot extends Thread {
    private BlockingQueue<Long> input, output;

    public HullPaintingRobot(BlockingQueue<Long> input, BlockingQueue<Long> output) {
        this.input = input;
        this.output = output;
    }

    public void run() {
        System.out.println("Robot started...");
        Map<Point, Long> hullMap = new HashMap<>();
        Map<Point, Boolean> hullIsPaintedMap = new HashMap<>();
        var x = 0;
        var y = 0;
        var currentDirection = 0;
        while(true) {
            var currentPosition = new Point(x,y);
            hullMap.computeIfAbsent(currentPosition, v -> 1L);
            input.add(hullMap.get(currentPosition));

            Long turnDirection = null;
            Long newColour = null;
            try {
                newColour = output.poll(1, TimeUnit.DAYS);
                if (newColour != null && newColour == 99) {
                    break;
                }
                turnDirection = output.poll(1, TimeUnit.DAYS);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Long finalNewColour = newColour;

            if (hullMap.containsKey(currentPosition) && !hullMap.get(currentPosition).equals(finalNewColour)) {
                hullIsPaintedMap.computeIfAbsent(currentPosition, v -> true);
            }

            hullMap.computeIfPresent(currentPosition, (k, v) -> v = finalNewColour );

            var nextDirection = getNextDirection(currentDirection, turnDirection);
            currentDirection = nextDirection;
            if (nextDirection == 0) {
                y--;
            } else if (nextDirection == 90) {
                x++;
            } else if (nextDirection == 180) {
                y++;
            } else if (nextDirection ==  270) {
                x--;
            }
        }

        System.out.println("Times painted: " + (hullIsPaintedMap.size()));
        System.out.println("Robot finished.");
        display(hullMap);
    }

    private void display(Map<Point, Long> hullMap) {
        var maxX = (int) hullMap.entrySet().stream()
                .max(Comparator.comparingInt(e -> (int) e.getKey().getX()))
                .get()
                .getKey().getX();
        var maxY = (int) hullMap.entrySet().stream()
                .max(Comparator.comparingInt(e -> (int) e.getKey().getY()))
                .get()
                .getKey().getY();

        String[][] grid = new String[maxY+1][maxX+1];

        for (int i=0; i < grid.length; i++) {
            for(int j=0; j < grid[i].length; j++) {
                grid[i][j] = " ";
            }

        }

        for (Map.Entry<Point, Long> point : hullMap.entrySet()) {
            var y = (int) point.getKey().getY();
            var x = (int) point.getKey().getX();
            if (point.getValue() == 0) {
                grid[y][x] = " ";
            } else {
                grid[y][x] = "#";
            }
        }

        for (int i=0; i < grid.length; i++) {
            for(int j=0; j < grid[i].length; j++) {
                System.out.print(grid[i][j]);
            }
            System.out.println();
        }
    }

    private int getNextDirection(int currentDirection, long nextDirection) {
        if (nextDirection == 0) {
            if (currentDirection == 0) {
                currentDirection = 270;
            } else {
                currentDirection -= 90;
            }
        } else {
            if (currentDirection == 270) {
                currentDirection = 0;
            } else {
                currentDirection +=90;
            }
        }
        return currentDirection;
    }

}
