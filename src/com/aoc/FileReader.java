package com.aoc;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FileReader {

    public static List<String> readFile(String filename) throws IOException {
        return Files.lines(Paths.get("./resources/" + filename)).collect(Collectors.toList());
    }

    public static List<Long> getIntCodeInput(String filename) throws IOException {
        String[] inputString = readFile(filename).get(0).split(",");
        return Arrays.stream(inputString).map(Long::parseLong).collect(Collectors.toList());
    }
}
