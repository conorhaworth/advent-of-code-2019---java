package com.aoc.day3;

import com.aoc.FileReader;

import java.awt.*;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class Challenge1and2 {

    public int challenge1and2() throws IOException {
        List<String> wires = FileReader.readFile("day3");
        var wire1Coords = plotPath(wires.get(0));
        var wire2Coords = plotPath(wires.get(1));
        var intersections = new ArrayList<Integer>();
        var stepsPerIntersection = new ArrayList<Integer>();

        for (Map.Entry<Point, Integer> coords : wire1Coords.entrySet()) {
            if (wire2Coords.containsKey(coords.getKey())) {
                intersections.add(Math.abs(coords.getKey().x) + Math.abs(coords.getKey().y));
                stepsPerIntersection.add(coords.getValue() + wire2Coords.get(coords.getKey()));

            }
        }
        var lowestDistance = intersections.stream().min(Comparator.comparing(Integer::valueOf));
        var leastAmountOfSteps = stepsPerIntersection.stream().min(Comparator.comparing(Integer::valueOf));

        System.out.println("Shortest distance: " + lowestDistance.get());
        System.out.println("Least amount of steps: " + leastAmountOfSteps.get());

        return 0;
    }

    private Map<Point, Integer> plotPath(String wire) {
        var coords = new LinkedHashMap<Point, Integer>();
        var currentX = 0;
        var currentY = 0;
        var step = 0;

        for(String wireDirection : wire.split(",")) {
            var direction = wireDirection.charAt(0);
            var distance = Integer.parseInt(wireDirection.substring(1));
            var dX = 0;
            var dY = 0;
            switch (direction) {
                case 'R' -> dX = 1;
                case 'L' -> dX = -1;
                case 'U' -> dY = 1;
                case 'D' -> dY = -1;
            };

            for (int i = 0; i < distance; i++) {
                coords.put(new Point(currentX += dX, currentY += dY), step++);
            }
        }
        return coords;
    }

}
