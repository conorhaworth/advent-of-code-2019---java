package com.aoc.day8;

import com.aoc.FileReader;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Challenge1and2 {
    private final static int IMAGE_WIDTH = 25;
    private final static int IMAGE_HEIGHT = 6;
    private final static int BLACK = 0;
    private final static int WHITE = 1;
    private final static int TRANSPARENT = 2;

    public List<int[][]> challenge1() throws IOException {
        List<Integer> pixels = Arrays.stream(FileReader.readFile("day8")
                .get(0)
                .split(""))
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        List<int[][]> layers = new ArrayList<>();

        var pixelCount = 0;
        while (pixelCount < pixels.size()) {
            var image = new int[IMAGE_HEIGHT][IMAGE_WIDTH];
            for (int x = 0; x < IMAGE_HEIGHT; x++) {
                for (int y = 0; y < IMAGE_WIDTH; y++) {
                    image[x][y] = pixels.get(pixelCount);
                    pixelCount++;
                }
            }
            layers.add(image);
        }

        var layerZeroCount = new HashMap<int[][], Integer>();
        for (int[][] layer : layers) {
            var zeroCount = 0;
            for (int x = 0; x < layer.length; x++) {
                for (int y = 0; y < layer[x].length; y++) {
                    if (layer[x][y] == 0) {
                        zeroCount++;
                    }
                }
            }
            layerZeroCount.put(layer, zeroCount);
        }

        var fewestZeroCountLayer = layerZeroCount.entrySet().stream()
                .min(Comparator.comparingInt(Map.Entry::getValue))
                .get()
                .getKey();

        var oneCount = 0;
        var twoCount = 0;
        for (int x = 0; x < fewestZeroCountLayer.length; x++) {
            for (int y = 0; y < fewestZeroCountLayer[x].length; y++) {
                if (fewestZeroCountLayer[x][y] == 1) {
                    oneCount++;
                } else if (fewestZeroCountLayer[x][y] == 2) {
                    twoCount++;
                }
            }
        }

        System.out.println(oneCount * twoCount);
        return layers;
    }

    /*
        pixel colours:
            0: black
            1: white
            2: transparent
     */

    public void challenge2() throws IOException {
        var layers = challenge1();

        var finalImage = new int[IMAGE_HEIGHT][IMAGE_WIDTH];
        for (int x = 0; x < IMAGE_HEIGHT; x++) {
            Arrays.fill(finalImage[x], TRANSPARENT);
        }

        for (int[][] layer : layers) {
            for (int x = 0; x < IMAGE_HEIGHT; x++) {
                for (int y = 0; y < IMAGE_WIDTH; y++) {
                    var pixel = layer[x][y];
                    var finalImagePixel = finalImage[x][y];

                    if (finalImagePixel == 2) {
                        finalImage[x][y] = pixel;
                    }
                }
            }
        }

        for (int x = 0; x < IMAGE_HEIGHT; x++) {
            for (int y = 0; y < IMAGE_WIDTH; y++) {
                if(finalImage[x][y] == BLACK || finalImage[x][y] == TRANSPARENT) {
                    System.out.print(" ");
                } else {
                    System.out.print("#");
                }
            }
            System.out.println();
        }

    }
}
