package com.aoc;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/*    Opcodes:
        1: add next two values
        2: multiply next two values
        3: takes a single integer as input and saves it to the address given by its only parameter
        4: outputs the value of its only parameter

       Parameter modes:
        0: position mode - causes the parameter to be interpreted as a position - if the parameter is 50, its value is the value stored at address 50 in memory
        1: immediate mode - a parameter is interpreted as a value - if the parameter is 50, its value is simply 50
*/

public class IntCode extends Thread {
    private List<Long> intCode;
    private int relativeBase = 0;
    private BlockingQueue<Long> input, output;

    public IntCode(List<Long> intCode, BlockingQueue<Long> input, BlockingQueue<Long> output) {
        Long[] extraMemory = new Long[intCode.size()*10];
        Arrays.fill(extraMemory, 0L);
        intCode.addAll(intCode.size(), Arrays.asList(extraMemory));
        this.intCode = intCode;
        this.input = input;
        this.output = output;
    }

    public void run() {
        System.out.println("IntCode started...");
        var instructionPointer = 0;
        while (instructionPointer < intCode.size()) {
            long value = intCode.get(instructionPointer);
            int opCode = Math.toIntExact(value % 100);
            long parameterMode1 = (value / 100) % 10;
            long parameterMode2 = (value / 1000) % 10;
            long parameterMode3 = (value / 10000) % 10;
            if (opCode == 99) {
                output.add((long) opCode);
                break;
            }
            try {
                switch(opCode) {
                    case 1 -> { addParameters(instructionPointer, parameterMode1, parameterMode2, parameterMode3); instructionPointer += 4; }
                    case 2 -> { multiplyParameters(instructionPointer, parameterMode1, parameterMode2, parameterMode3); instructionPointer += 4; }
                    case 3 -> { saveInput(instructionPointer, parameterMode1); instructionPointer += 2; }
                    case 4 -> { outputParameter(instructionPointer, parameterMode1); instructionPointer += 2; }
                    case 5 -> instructionPointer = jumpIfTrue(instructionPointer, parameterMode1, parameterMode2);
                    case 6 -> instructionPointer = jumpIfFalse(instructionPointer, parameterMode1, parameterMode2);
                    case 7 -> { lessThan(instructionPointer, parameterMode1, parameterMode2, parameterMode3); instructionPointer += 4; }
                    case 8 -> { equals(instructionPointer, parameterMode1, parameterMode2, parameterMode3); instructionPointer += 4; }
                    case 9 -> { adjustRelativeBase(instructionPointer, parameterMode1); instructionPointer += 2; }
                    default -> throw new Exception("Invalid OpCode: " + opCode);
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        System.out.println("IntCode finished.");
    }

    private void adjustRelativeBase(int instructionPointer, long parameterMode1) {
        relativeBase += getParameterRead(parameterMode1, instructionPointer+1);
    }

    private void lessThan(int instructionPointer, long parameterMode1, long parameterMode2, long parameterMode3) throws Exception {
        var parameter1 = getParameterRead(parameterMode1, instructionPointer+1);
        var parameter2 = getParameterRead(parameterMode2, instructionPointer+2);
        var parameter3 = getParameterWrite(parameterMode3, instructionPointer+3);
        if (parameter1 < parameter2) {
            intCode.set(Math.toIntExact(parameter3), 1L);
        } else {
            intCode.set(Math.toIntExact(parameter3), 0L);
        }
    }

    private void equals(int instructionPointer, long parameterMode1, long parameterMode2, long parameterMode3) throws Exception {
        var parameter1 = getParameterRead(parameterMode1, instructionPointer+1);
        var parameter2 = getParameterRead(parameterMode2, instructionPointer+2);
        var parameter3 = getParameterWrite(parameterMode3, instructionPointer+3);
        if (parameter1 == parameter2) {
            intCode.set(Math.toIntExact(parameter3), 1L);
        } else {
            intCode.set(Math.toIntExact(parameter3), 0L);
        }
    }

    private int jumpIfTrue(int instructionPointer, long parameterMode1, long parameterMode2) {
        if(getParameterRead(parameterMode1, instructionPointer+1) != 0) {
            instructionPointer = Math.toIntExact(getParameterRead(parameterMode2, instructionPointer + 2));
        } else {
            instructionPointer += 3;
        }
        return instructionPointer;
    }

    private int jumpIfFalse(int instructionPointer, long parameterMode1, long parameterMode2) {
        if(getParameterRead(parameterMode1, instructionPointer+1) == 0) {
            instructionPointer = Math.toIntExact(getParameterRead(parameterMode2, instructionPointer + 2));
        } else {
            instructionPointer += 3;
        }
        return instructionPointer;
    }

    private void outputParameter(int instructionPointer, long parameterMode) {
        var parameter = getParameterRead(parameterMode, instructionPointer + 1);
        output.add(parameter);
//        System.out.println("Intcode output: " + parameter);
    }

    private void saveInput(int instructionPointer, long parameterMode) throws Exception {
        intCode.set(Math.toIntExact(getParameterWrite(parameterMode, instructionPointer+1)), input.poll(1, TimeUnit.DAYS));
    }

    private void multiplyParameters(int instructionPointer, long parameterMode1, long parameterMode2, long parameterMode3) throws Exception {
        var parameter1 = getParameterRead(parameterMode1, instructionPointer+1);
        var parameter2 = getParameterRead(parameterMode2, instructionPointer+2);
        var parameter3 = getParameterWrite(parameterMode3, instructionPointer+3);
        var result = parameter1 * parameter2;
        intCode.set(Math.toIntExact(parameter3), result);
    }

    private void addParameters(int instructionPointer, long parameterMode1, long parameterMode2, long parameterMode3) throws Exception {
        var parameter1 = getParameterRead(parameterMode1, instructionPointer+1);
        var parameter2 = getParameterRead(parameterMode2, instructionPointer+2);
        var parameter3 = getParameterWrite(parameterMode3, instructionPointer+3);
        var result = parameter1 + parameter2;
        intCode.set(Math.toIntExact(parameter3), result);
    }

    private long getParameterRead(long parameterMode, int instructionPointer) {
        var parameter = 0L;
        if (parameterMode == 0) {
            parameter = intCode.get(Math.toIntExact(intCode.get(instructionPointer)));
        } else if (parameterMode == 1) {
            parameter = intCode.get(instructionPointer);
        } else if (parameterMode == 2) {
            parameter = intCode.get(Math.toIntExact(intCode.get(instructionPointer)) + relativeBase);
        }
        return parameter;
    }

    private long getParameterWrite(long parameterMode, int instructionPointer) throws Exception {
        var parameter = 0L;
        if (parameterMode == 0) {
            parameter = intCode.get(instructionPointer);
        } else if (parameterMode == 1) {
            throw new Exception("Invalid parameter mode for write: " + parameterMode);
        } else if (parameterMode == 2) {
            parameter = intCode.get(instructionPointer) + relativeBase;
        }
        return parameter;
    }
}
