package com.aoc.day13;

import com.aoc.FileReader;
import com.aoc.IntCode;

import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class Challenge1and2 {

    public void challenge1() throws IOException, InterruptedException {
        var memory = FileReader.getIntCodeInput("day13");
        ArrayBlockingQueue<Long> input = new ArrayBlockingQueue<>(100000);
        ArrayBlockingQueue<Long> output = new ArrayBlockingQueue<>(100000);
        memory.set(0, 2L);
        IntCode intCode = new IntCode(memory, input, output);
        String[][] display = new String[25][40];

        intCode.start();

        while (true) {
            var x = Math.toIntExact(output.poll(1, TimeUnit.DAYS));
            if (x == 99) {
                break;
            }
            var y = Math.toIntExact(output.poll(1, TimeUnit.DAYS));
            var id = Math.toIntExact(output.poll(1, TimeUnit.DAYS));

            switch (id) {
                case 0 -> display[y][x] = " ";
                case 1 -> display[y][x] = "#";
                case 2 -> display[y][x] = "X";
                case 3 -> display[y][x] = "-";
                case 4 -> display[y][x] = "o";
            }
        }
        var tileCount = 0;
        for (int y=0; y<display.length; y++) {
            for (int x=0; x<display[y].length; x++) {
                System.out.print(display[y][x]);
                if (display[y][x].equals("X")) {
                    tileCount++;
                }
            }
            System.out.println();
        }
        System.out.println("Tiles left: " + tileCount);
    }
}
//    0 is an empty tile. No game object appears in this tile.
//            1 is a wall tile. Walls are indestructible barriers.
//            2 is a block tile. Blocks can be broken by the ball.
//            3 is a horizontal paddle tile. The paddle is indestructible.
//            4 is a ball tile. The ball moves diagonally and bounces off objects.
