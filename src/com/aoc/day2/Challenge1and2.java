package com.aoc.day2;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.aoc.FileReader.*;

public class Challenge1and2 {

    public List<Integer> challenge1(int noun, int verb) throws IOException {
        String[] inputString = readFile("day2").get(0).split(",");
        List<Integer> intCode = Arrays.stream(inputString).map(Integer::parseInt).collect(Collectors.toList());

        intCode.set(1, noun);
        intCode.set(2, verb);

        var halt = false;
        var instructionPointer = 0;
        while (!halt) {
            var value = intCode.get(instructionPointer);
            if (value == 99) {
                halt = true;
            }
            var parameter1 = intCode.get(intCode.get(instructionPointer+1));
            var parameter2 = intCode.get(intCode.get(instructionPointer+2));
            var result = 0;
            if (value == 1) {
                result = parameter1 + parameter2;
                
            } else if (value == 2) {
                result = parameter1 * parameter2;
            }
            var resultAddress = intCode.get(instructionPointer+3);
            intCode.set(resultAddress, result);
            instructionPointer += 4;
        }
        return intCode;
    }

    public int challenge2() throws IOException {
        var desiredResult = 19690720;
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                var result = challenge1(i, j);
                if (result.get(0) == desiredResult) {
                    return 100 * i + j;
                }
            }
        }
        return 0;
    }
    
}
