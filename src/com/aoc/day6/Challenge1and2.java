package com.aoc.day6;

import com.aoc.FileReader;

import java.io.IOException;
import java.util.*;

public class Challenge1and2 {
    private Map<String, List<String>> orbitMap = new HashMap<>();
    private Tree<String> orbitRelations = new Tree<>();
    private Tree.Node<String> myNode;
    private Tree.Node<String> santasNode;

    public void challenge1() throws IOException {
        initTree();
        var totalOrbits = getTotalOrbits(orbitRelations.getRoot(), 0, 0);
        System.out.println(totalOrbits);
    }

    public void challenge2() throws IOException {
        initTree();
        findMeAndSanta(orbitRelations.getRoot());
        var myParents = findAllParents(myNode, new LinkedHashMap<>(), 0);
        var santasParents = findAllParents(santasNode, new LinkedHashMap<>(), 0);
        var distanceBetweenNodes = findDistanceBetweenNodes(myParents, santasParents);

        System.out.println(distanceBetweenNodes);
    }

    private void initTree() throws IOException {
        List<String> orbits = FileReader.readFile("day6");

        orbitRelations.setRoot(new Tree.Node<>("COM"));

        for (String orbit : orbits) {
            var objects = orbit.split("\\)");
            List<String> objectList = orbitMap.computeIfAbsent(objects[0], s -> new ArrayList<>());
            objectList.add(objects[1]);
        }

        insertChildren(orbitRelations.getRoot());
    }

    private void insertChildren(Tree.Node<String> parent) {
        List<String> children = orbitMap.get(parent.getData());
        for (String child : children) {
            var childNode = new Tree.Node<>(child);
            childNode.setParent(parent);
            parent.addChild(childNode);
            if (orbitMap.containsKey(child)) {
                insertChildren(childNode);
            }
        }
    }

    private int getTotalOrbits(Tree.Node<String> parent, int nodeDepth, int totalOrbits) {
        if (parent.hasChildren()) {
            for (Tree.Node<String> child : parent.getChildren()) {
                totalOrbits += ++nodeDepth;
                totalOrbits = getTotalOrbits(child, nodeDepth, totalOrbits);
                nodeDepth--;
            }
        }
        return totalOrbits;
    }

    private void findMeAndSanta(Tree.Node<String> parent) {
        if (parent.hasChildren()) {
            for (Tree.Node<String> child : parent.getChildren()) {
                if (child.getData().equals("YOU")) {
                    myNode = child;
                }
                if (child.getData().equals("SAN")) {
                    santasNode = child;
                }
                if (myNode == null || santasNode == null) {
                    findMeAndSanta(child);
                }
            }
        }
    }

    public Map<Tree.Node<String>, Integer> findAllParents(Tree.Node<String> originNode, Map<Tree.Node<String>, Integer> parents, int distance) {
        if (originNode.hasParent()) {
            distance++;
            parents.put(originNode.getParent(), distance);
            findAllParents(originNode.getParent(), parents, distance);
        }
        return parents;
    }

    public int findDistanceBetweenNodes(Map<Tree.Node<String>, Integer> firstNodeParents, Map<Tree.Node<String>, Integer> secondNodeParents) {
        for (Map.Entry<Tree.Node<String>, Integer> parent : firstNodeParents.entrySet()) {
            if (secondNodeParents.containsKey(parent.getKey())) {
                return parent.getValue() + secondNodeParents.get(parent.getKey()) - 2;
            }
        }
        return 0;
    }

}

class Tree<T> {
    private Node<T> root;

    public void setRoot(Node<T> rootData) { root = rootData; }

    public Node<T> getRoot() { return root; }

    public static class Node<T> {
        private T data;
        private Node<T> parent;
        private List<Node<T>> children;

        public Node(T data) {
            this.children = new ArrayList<>();
            this.data = data;
        }

        public boolean hasParent() { return parent != null; }

        public void setParent(Node<T> parent) { this.parent = parent; }

        public Node<T> getParent() { return parent; }

        public boolean hasChildren() { return !children.isEmpty(); }

        public void addChild(Node<T> child) { children.add(child); }

        public List<Node<T>> getChildren() { return children; }

        public T getData() { return data; }
    }
}