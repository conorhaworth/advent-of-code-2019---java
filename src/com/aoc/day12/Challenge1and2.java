package com.aoc.day12;

import com.aoc.FileReader;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Challenge1and2 {
    private List<Moon> moons;
    private List<List<Moon>> states = new ArrayList<>();
    int steps = 0;


    public void challenge1() throws IOException {
        List<String> moonInput = FileReader.readFile("day12");
        moons = new ArrayList<>();
        for (var moon : moonInput) {
            moon = moon.replace(">", "");
            var parsedCoords = Arrays.stream(moon.split(",")).map(s -> s.substring(s.indexOf('=')+1)).collect(Collectors.toList());
            var position = new Point3D(Integer.parseInt(parsedCoords.get(0)),Integer.parseInt(parsedCoords.get(1)),Integer.parseInt(parsedCoords.get(2)));
            var velocity = new Point3D(0,0,0);
            moons.add(new Moon(position, velocity));
        }

        for (int i=0; true; i++) {
            timeStep();
            System.out.println("After " + (i+1) + " steps: ");
//            moons.forEach(System.out::println);
        }

//        var totalSystemEnergy = 0;
//        for (Moon moon : moons) {
//            var potentialEnergy = Math.abs(moon.getPosition().getX()) + Math.abs(moon.getPosition().getY()) + Math.abs(moon.getPosition().getZ());
//            var kineticEnergy = Math.abs(moon.getVelocity().getX()) + Math.abs(moon.getVelocity().getY()) + Math.abs(moon.getVelocity().getZ());
//            moon.setPotentialEnergy(potentialEnergy);
//            moon.setKineticEnergy(kineticEnergy);
//            moon.setTotalEnergy(potentialEnergy*kineticEnergy);
//            totalSystemEnergy += moon.getTotalEnergy();
//        }
//        System.out.println(totalSystemEnergy);
    }

    private void timeStep() {
        steps++;
        //apply gravity
        applyGravity();

        //apply velocity
//        for (Moon moon : moons) {
//            moon.getPosition().setY(moon.getPosition().getY() + moon.getVelocity().getY());
//            moon.getPosition().setZ(moon.getPosition().getZ() + moon.getVelocity().getZ());
//            moon.getPosition().setX(moon.getPosition().getX() + moon.getVelocity().getX());
//        }

        var matchesPreviousState = false;
        //check states
        if (states.size() > 1) {
            for (int i=0; i < states.size(); i++) {
                for (int j=0; j < moons.size(); j++) {
                    matchesPreviousState = moons.get(j).equals(states.get(i).get(j));
                }
            }
        }

        if (matchesPreviousState) {
            System.out.println();
            System.exit(0);
        }

        List<Moon> moonsCopy = new ArrayList<>();
        moons.forEach(m -> moonsCopy.add((Moon) m.clone()));
        states.add(moonsCopy);
    }

    private void applyGravity() {
        for (int i=0; i<moons.size(); i++) {
            for (int j=i+1; j < moons.size(); j++) {
                var moon1 = moons.get(i);
                var moon2 = moons.get(j);
                if (moon1.getPosition().getX() > moon2.getPosition().getX()) {
                    moon1.getVelocity().setX(moon1.getVelocity().getX()-1);
                    moon2.getVelocity().setX(moon2.getVelocity().getX()+1);
                } else if (moon1.getPosition().getX() < moon2.getPosition().getX()) {
                    moon1.getVelocity().setX(moon1.getVelocity().getX()+1);
                    moon2.getVelocity().setX(moon2.getVelocity().getX()-1);
                }

                if (moon1.getPosition().getY() > moon2.getPosition().getY()) {
                    moon1.getVelocity().setY(moon1.getVelocity().getY()-1);
                    moon2.getVelocity().setY(moon2.getVelocity().getY()+1);
                } else if (moon1.getPosition().getY() < moon2.getPosition().getY()) {
                    moon1.getVelocity().setY(moon1.getVelocity().getY()+1);
                    moon2.getVelocity().setY(moon2.getVelocity().getY()-1);
                }

                if (moon1.getPosition().getZ() > moon2.getPosition().getZ()) {
                    moon1.getVelocity().setZ(moon1.getVelocity().getZ()-1);
                    moon2.getVelocity().setZ(moon2.getVelocity().getZ()+1);
                } else if (moon1.getPosition().getZ() < moon2.getPosition().getZ()) {
                    moon1.getVelocity().setZ(moon1.getVelocity().getZ()+1);
                    moon2.getVelocity().setZ(moon2.getVelocity().getZ()-1);
                }
            }
            var currentMoon = moons.get(i);
            currentMoon.getPosition().setY(currentMoon.getPosition().getY() + currentMoon.getVelocity().getY());
            currentMoon.getPosition().setZ(currentMoon.getPosition().getZ() + currentMoon.getVelocity().getZ());
            currentMoon.getPosition().setX(currentMoon.getPosition().getX() + currentMoon.getVelocity().getX());
        }
    }
}

class Moon {
    private Point3D position;
    private Point3D velocity;
    private int potentialEnergy;
    private int kineticEnergy;
    private int totalEnergy;

    public Moon(Point3D position, Point3D velocity) {
        this.position = position;
        this.velocity = velocity;
    }

    @Override
    protected Object clone() {
        var cloneVelocity = new Point3D(this.velocity.getX(), this.velocity.getY(), this.velocity.getZ());
        var clonePosition = new Point3D(this.position.getX(), this.position.getY(), this.position.getZ());
        Moon clone = new Moon(clonePosition, cloneVelocity);
        clone.setTotalEnergy(this.getTotalEnergy());
        clone.setKineticEnergy(this.getKineticEnergy());
        clone.setPotentialEnergy(this.getPotentialEnergy());
        return clone;
    }


    @Override
    public String toString() {
        return "Position: x->" + position.getX() + " y->" + position.getY() + " z->" + position.getZ() +
                " Velocity: x->" + velocity.getX() + " y->" + velocity.getY() + " z->" + velocity.getZ();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Moon) {
            var moon = (Moon) obj;
            return this.getPosition().equals(moon.getPosition()) && this.getVelocity().equals(moon.getVelocity());
        }
        return false;
    }

    public Point3D getPosition() {
        return position;
    }

    public void setPosition(Point3D position) {
        this.position = position;
    }

    public Point3D getVelocity() {
        return velocity;
    }

    public void setVelocity(Point3D velocity) {
        this.velocity = velocity;
    }

    public int getPotentialEnergy() {
        return potentialEnergy;
    }

    public void setPotentialEnergy(int potentialEnergy) {
        this.potentialEnergy = potentialEnergy;
    }

    public int getKineticEnergy() {
        return kineticEnergy;
    }

    public void setKineticEnergy(int kineticEnergy) {
        this.kineticEnergy = kineticEnergy;
    }

    public int getTotalEnergy() {
        return totalEnergy;
    }

    public void setTotalEnergy(int totalEnergy) {
        this.totalEnergy = totalEnergy;
    }
}


