package com.aoc.day1;

import com.aoc.FileReader;
import java.io.IOException;
import java.util.List;

public class Challenge1and2 {

    public int challenge1() throws IOException {
        List<String> masses = FileReader.readFile("day1");
        return masses.stream()
                .mapToInt(mass -> (Integer.parseInt(mass) / 3) - 2)
                .sum();
    }

    public int challenge2() throws IOException {
        List<String> masses = FileReader.readFile("day1");
        return masses.stream()
                .mapToInt(mass -> {
                    var fuelRequirement = (Integer.parseInt(mass) / 3) - 2;
                    return calcExtraFuel(fuelRequirement);
                })
                .sum();
    }

    private int calcExtraFuel(int mass) {
        var totalFuel = mass;
        var extraFuel = (mass / 3) - 2;

        if (extraFuel > 0) {
            totalFuel += calcExtraFuel(extraFuel);
        }
        return totalFuel;
    }
}
