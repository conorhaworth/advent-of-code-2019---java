package com.aoc.day10;

import com.aoc.FileReader;

import java.awt.*;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class Challenge1and2 {

    public static final String ASTEROID = "#";
    private int asteroidCount = 0;

    public void challenge1() throws IOException {
        List<String> lines = FileReader.readFile("day10");

        String[][] asteroidGrid = new String[lines.size()][lines.get(0).length()];

        for (int y = 0; y < lines.size(); y++) {
            var positions = lines.get(y).split("");
            for (int x = 0; x < positions.length; x++) {
                asteroidGrid[y][x] = positions[x];
                if (positions[x].equals(ASTEROID)) {
                    asteroidCount++;
                }
            }
        }

        Map<Point, Integer> asteroidSeenMap = new HashMap<>();
        for (int y = 0; y < asteroidGrid.length; y++) {
            for (int x = 0; x < asteroidGrid[y].length; x++) {
                var currentPosition = asteroidGrid[y][x];
                if (currentPosition.equals(ASTEROID)) {
                    var totalSeen = getTotalVisibleAsteroids(asteroidGrid, y, x);
                    asteroidSeenMap.put(new Point(x,y), totalSeen);
                }
            }
        }
        // 305 - too high, not 45
        var mostSeen = asteroidSeenMap.entrySet().stream()
                .max(Comparator.comparingInt(Map.Entry::getValue))
                .get()
                .getValue();

        System.out.println(mostSeen);


        for (int y = 0; y < asteroidGrid.length; y++) {
            for (int x = 0; x < asteroidGrid[y].length; x++) {
                System.out.print(asteroidGrid[y][x]);
            }
            System.out.println();
        }
    }

    private int getTotalVisibleAsteroids(String[][] asteroidGrid, int currentY, int currentX) {
        var asteroidsSeen = 0;
        List<Point> blockedPoints = new ArrayList<>();
        for (int y = 1; y < asteroidGrid.length-currentY; y++) {
            for (int x = 1; x < asteroidGrid[y].length-currentX; x++) {
                if (asteroidGrid[currentY+y][currentX+x].equals(ASTEROID) && !isBlocked(y, x, blockedPoints)){
                    //blocks everything behind it
                    blockedPoints.add(new Point(x,y));
                    asteroidsSeen++;
                }
            }
        }

        return asteroidsSeen;
    }

    private boolean isBlocked(int y, int x, List<Point> blockedPoints) {
        for (Point blockedPoint : blockedPoints) {
            if ((blockedPoint.x - x ==  blockedPoint.y - y) 
                || (blockedPoint.x == x)
                || (blockedPoint.y == y)) {
                return true;
            }
        }
        return false;
    }
}
